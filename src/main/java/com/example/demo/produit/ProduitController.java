package com.example.demo.produit;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class ProduitController {

	@Autowired
	private ProduitService produitService;

	@GetMapping({"/"})
	public List<Produit> getAllProduits() {
		return produitService.getAllProduits();
	}

	@GetMapping({"/oneProduit/{id}"})
	public Optional<Produit> getOneProduit(@PathVariable ObjectId id) {
		return produitService.getProduit(id);
	}

	@PutMapping({"/oneProduit/{id}"})
	public void updateOneProduit(@RequestBody Produit produit, @PathVariable ObjectId id) {
		produitService.updateProduit(id,produit);
	}
	@DeleteMapping({"/oneProduit/{id}"})
	public void deleteOneProduit(@PathVariable ObjectId id) {
		produitService.deleteProduit(id);
	}

	@GetMapping({"/categories/{categoriesId}/produits"})
	public List<Produit> getAllProduits(@PathVariable String categoriesId) {
		return produitService.getAllProduitsCategorie(categoriesId);
	}

	@GetMapping({"/categories/{categoriesId}/produits/{id}"})
	public Optional<Produit> getProduit(@PathVariable ObjectId id) {
		return produitService.getProduit(id);
	}

	@PostMapping({"/categories/{categoriesId}/produits"})
	@ResponseBody
	public void addProduit(@RequestBody List<Produit> produits, @PathVariable String categoriesId) {
		for (int i = 0; i< produits.size();i++) {
			produits.get(i).setCategory(categoriesId);
			produitService.addProduit(produits.get(i));
		}
	}


	@PutMapping({"/categories/{categoriesId}/produits/{id}"})
	public void updateProduit(@RequestBody Produit produit, @PathVariable String categoriesId, @PathVariable ObjectId id) {
		produit.setCategory(categoriesId);
		produitService.updateProduit(id,produit);
	}

	@DeleteMapping({"/categories/{categoriesId}/produits/{id}"})
	public void deleteProduit(@PathVariable ObjectId id) {
		produitService.deleteProduit(id);
	}
}
