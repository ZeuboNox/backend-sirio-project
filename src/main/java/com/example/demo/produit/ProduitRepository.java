package com.example.demo.produit;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProduitRepository extends MongoRepository<Produit, ObjectId> {

     public Optional<Produit> findById(ObjectId idProduit);
     public List<Produit> findByCategory(String idCategory);
}
