package com.example.demo.produit;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProduitService {
    @Autowired
    private ProduitRepository produitRepository;

    public List<Produit> getAllProduitsCategorie(String categoriesId) {
        List< Produit > products = new ArrayList();
        this.produitRepository.findByCategory(categoriesId).forEach(products::add);
        return products;
    }

    public List<Produit> getAllProduits() {
        List<Produit> produits = new ArrayList();
        this.produitRepository.findAll().forEach(produits::add);
        return produits;
    }

    public Optional<Produit> getProduit(ObjectId id) {
        return this.produitRepository.findById(id);
    }

    public void addProduit(Produit produits) {
        this.produitRepository.save(produits);
    }

    public void updateProduit(ObjectId id,Produit produits) {
        this.produitRepository.save(produits);
    }

    public void deleteProduit(ObjectId id) {
        this.produitRepository.deleteById(id);
    }


}
