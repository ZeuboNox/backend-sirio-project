package com.example.demo.commande;


import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CommandeRepository extends MongoRepository<Commande, ObjectId> {

    Optional<Commande> findById(ObjectId idCommande);
    List<Commande> findByUser(ObjectId idUtilisateur);
}
