package com.example.demo.commande;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CommandeService {
    @Autowired
    private CommandeRepository commandeRepository;

    public List<Commande> getAllCommandes(ObjectId userId) {
        List<Commande> commandes = new ArrayList();
        this.commandeRepository.findByUser(userId).forEach(commandes::add);
        return commandes;
    }

    public Optional<Commande> getCommande(ObjectId id) {
        return this.commandeRepository.findById(id);
    }

    public void addCommande(Commande commande) {
        this.commandeRepository.save(commande);
    }

    public void updateCommande(Commande commande) {
        this.commandeRepository.save(commande);
    }

    public void deleteCommande(ObjectId id) {
        this.commandeRepository.deleteById(id);
    }
}
