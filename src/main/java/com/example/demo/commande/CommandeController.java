package com.example.demo.commande;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CommandeController {

	@Autowired
	private CommandeService commandeService;

	@GetMapping({"/utilisateurs/{userId}/commandes"})
	public List<Commande> getAllCommandes(@PathVariable ObjectId userId) {
		return this.commandeService.getAllCommandes(userId);
	}

	@GetMapping({"/utilisateurs/{userId}/commandes/{id}"})
	public Optional<Commande> getCommande(@PathVariable ObjectId id) {
		return this.commandeService.getCommande(id);
	}

	@PostMapping({"/utilisateurs/{userId}/commandes"})
	@ResponseBody
	public void addCommande(@RequestBody List<Commande> commandes, @PathVariable ObjectId userId) {
		for (int i = 0; i< commandes.size();i++) {
			commandes.get(i).setUser(userId);
			this.commandeService.addCommande(commandes.get(i));
		}
	}

	@PutMapping({"/utilisateurs/{userId}/commandes/{id}"})
	public void updateCommande(@RequestBody Commande commande, @PathVariable ObjectId userId) {
		commande.setUser(userId);
		this.commandeService.updateCommande(commande);
	}

	@DeleteMapping({"/utilisateurs/{userId}/commandes/{id}"})
	public void deleteCommande(@PathVariable ObjectId id) {
		this.commandeService.deleteCommande(id);
	}

}
