package com.example.demo.commande;


import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.lang.reflect.Array;
import java.util.Date;
@Document(collection="orders")
public class Commande {
	@Id
	private ObjectId id;
	private Array cart;
	private Boolean delivered;
	private Boolean paid;
	private ObjectId user;
	private String address;
	private String mobile;
	private Float total;
	private Date dateOfPaiement;
	private String method;
	private String paymentId;

	public Commande(ObjectId id, Array cart, Boolean delivered, Boolean paid, ObjectId user, String address, String mobile, Float total, Date dateOfPaiement, String method, String paymentId) {
		this.id = id;
		this.cart = cart;
		this.delivered = delivered;
		this.paid = paid;
		this.user = user;
		this.address = address;
		this.mobile = mobile;
		this.total = total;
		this.dateOfPaiement = dateOfPaiement;
		this.method = method;
		this.paymentId = paymentId;
	}

	public Commande() {
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public Array getCart() {
		return cart;
	}

	public void setCart(Array cart) {
		this.cart = cart;
	}

	public Boolean getDelivered() {
		return delivered;
	}

	public void setDelivered(Boolean delivered) {
		this.delivered = delivered;
	}

	public Boolean getPaid() {
		return paid;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	public ObjectId getUser() {
		return user;
	}

	public void setUser(ObjectId user) {
		this.user = user;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Float getTotal() {
		return total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Date getDateOfPaiement() {
		return dateOfPaiement;
	}

	public void setDateOfPaiement(Date dateOfPaiement) {
		this.dateOfPaiement = dateOfPaiement;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
}
