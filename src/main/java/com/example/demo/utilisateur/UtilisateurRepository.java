package com.example.demo.utilisateur;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UtilisateurRepository extends MongoRepository<Utilisateur, ObjectId> {

    public Optional<Utilisateur> findById(ObjectId idUtilisateur);
    public List<Utilisateur> findByRole(String role);
    public List<Utilisateur> findByRoot(Boolean root);
    public List<Utilisateur> findByAvatar(String avatar);
    public List<Utilisateur> findByName(String name);
    public List<Utilisateur> findByEmail(String email);
    public List<Utilisateur> findByPassword(String password);
    public List<Utilisateur> findByCreatedAt(String createdAt);
    public List<Utilisateur> findByUpdatedAt(String updatedAt);

}
