package com.example.demo.utilisateur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class Password {
	@Autowired
	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

	public String encrypt(String password) {
		String encodedPassword = passwordEncoder.encode(password);
		return encodedPassword;
	}
	public boolean isPasswordMatch(String password,String encodedPassword) {

		boolean isPasswordMatch = passwordEncoder.matches(password, encodedPassword);
		return isPasswordMatch;
	}
}