package com.example.demo.utilisateur;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UtilisateurService {
    @Autowired
    private UtilisateurRepository utilisateurRepository;
    BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    public List<Utilisateur> getAllUtilisateurs() {
        List<Utilisateur> utilisateurs = new ArrayList();
        this.utilisateurRepository.findAll().forEach(utilisateurs::add);
        return utilisateurs;
    }

    public Optional<Utilisateur> getUtilisateur( ObjectId id ) {
        return this.utilisateurRepository.findById(id);
    }

    public void addUtilisateur(Utilisateur utilisateur) {
        String psw = encrypt(utilisateur.getPassword());
        utilisateur.setPassword(psw);
        this.utilisateurRepository.save(utilisateur);
    }

    public void updateUtilisateur(ObjectId id,Utilisateur utilisateur) {
        String psw = encrypt(utilisateur.getPassword());
        utilisateur.setPassword(psw);
        this.utilisateurRepository.save(utilisateur);
    }

    public void deleteUtilisateur(ObjectId id) {
        this.utilisateurRepository.deleteById(id);
    }

    public Utilisateur getAccesAndUtilisateur(String email, String psw) {
        List< Utilisateur > utilisateurs = new ArrayList();
        this.utilisateurRepository.findByEmail(email).forEach(utilisateurs::add);
        for (int i = 0; i < utilisateurs.size(); i++) {
            if (isPasswordMatch(psw, utilisateurs.get(i).getPassword())) {
                return utilisateurs.get(i);
            }
        }
        return null;
    }


    public String encrypt(String password) {
        String encodedPassword = passwordEncoder.encode(password);
        return encodedPassword;
    }
    public boolean isPasswordMatch(String password,String encodedPassword) {

        boolean isPasswordMatch = passwordEncoder.matches(password, encodedPassword);
        return isPasswordMatch;
    }
}
