package com.example.demo.utilisateur;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class UtilisateurController {

	@Autowired
	private UtilisateurService utilisateurService;

	@GetMapping({"/utilisateurs"})
	public List<Utilisateur> getAllUtilisateurs() {
		return this.utilisateurService.getAllUtilisateurs();
	}

	@GetMapping({"/connexion"})
	public Utilisateur getAccesAndUtilisateur(
			@RequestParam(name = "email",required = true) String email,
			@RequestParam(name = "password",required = true) String password
	) {
		return utilisateurService.getAccesAndUtilisateur(email,password);
	}

	@GetMapping({"/utilisateurs/{id}"})
	public Optional<Utilisateur> getUtilisateur(@PathVariable ObjectId id) {
		return this.utilisateurService.getUtilisateur(id);
	}
	@PostMapping({"/utilisateurs"})
	@ResponseBody
	public void addUtilisateur(@RequestBody List<Utilisateur> utilisateurs) {
		for (int i = 0; i< utilisateurs.size();i++) {
			this.utilisateurService.addUtilisateur(utilisateurs.get(i));
		}
	}

	@PutMapping({"/utilisateurs/{id}"})
	public void updateUtilisateur(@RequestBody Utilisateur utilisateur, @PathVariable ObjectId id) {
		this.utilisateurService.updateUtilisateur(id,utilisateur);
	}

	@DeleteMapping({"/utilisateurs/{id}"})
	public void deleteUtilisateur(@PathVariable ObjectId id) {
		this.utilisateurService.deleteUtilisateur(id);
	}
}
