package com.example.demo.utilisateur;


import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.auditing.CurrentDateTimeProvider;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.annotation.Generated;
import java.io.ObjectInput;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

@Document(collection="users")
public class Utilisateur {
	@Id
	private ObjectId id;
	private String role;
	private Boolean root;
	private String avatar;
	private String name;
	private String email;
	private String password;
	private Date createdAt;
	private Date updatedAt;

	public Utilisateur(ObjectId id, String role, Boolean root, String avatar, String name, String email, String password, Date updatedAt) {
		this.id = id;
		this.role = role;
		this.root = root;
		this.avatar = avatar;
		this.name = name;
		this.email = email;
		this.password = password;
		this.updatedAt = updatedAt;
	}

	public Utilisateur() {
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Boolean getRoot() {
		return root;
	}

	public void setRoot(Boolean root) {
		this.root = root;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}


}
