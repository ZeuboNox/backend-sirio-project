package com.example.demo.categorie;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class CategorieController {

	@Autowired
	private CategorieService categorieService;

	@GetMapping({"/categories"})
	public List<Categorie> getAllCategories() {
		return this.categorieService.getAllCategories();
	}

	@GetMapping({"/categories/{id}"})
	public Optional<Categorie> getCategorie(@PathVariable ObjectId id) {
		return this.categorieService.getCategorie(id);
	}

	@PostMapping({"/categories"})
	@ResponseBody
	public void addCategorie(@RequestBody List<Categorie> categories) {
		for (int i = 0; i< categories.size();i++) {
			this.categorieService.addCategorie(categories.get(i));
		}
	}

	@PutMapping({"/categories/{id}"})
	public void updateCategorie(@RequestBody Categorie categorie, @PathVariable ObjectId id) {
		this.categorieService.updateCategorie(id,categorie);
	}

	@DeleteMapping({"/categories/{id}"})
	public void deleteCategorie(@PathVariable ObjectId id) {
		this.categorieService.deleteCategorie(id);
	}
}
