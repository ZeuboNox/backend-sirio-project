package com.example.demo.categorie;


import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CategorieService {
    @Autowired
    private CategorieRepository categorieRepository;

    public List<Categorie> getAllCategories() {
        List<Categorie> categories = new ArrayList();
        this.categorieRepository.findAll().forEach(categories::add);
        return categories;
    }

    public Optional<Categorie> getCategorie(ObjectId id) {
        return this.categorieRepository.findById(id);
    }

    public void addCategorie(Categorie categorie) {
        this.categorieRepository.save(categorie);
    }

    public void updateCategorie(ObjectId id, Categorie categorie) {
        this.categorieRepository.save(categorie);
    }

    public void deleteCategorie(ObjectId id) {
        this.categorieRepository.deleteById(id);
    }
}
