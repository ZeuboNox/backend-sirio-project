package com.example.demo.categorie;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection="categories")
public class Categorie {

	@Id
	private ObjectId id;
	private String name;
	private Date createdAt;
	private Date updatedAt;

	public Categorie(ObjectId id, String name, Date updatedAt) {
		this.id = id;
		this.name = name;
		this.updatedAt = updatedAt;
	}

	public Categorie() {
	}

	public ObjectId getId() {
		return id;
	}

	public void setId(ObjectId id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
}
