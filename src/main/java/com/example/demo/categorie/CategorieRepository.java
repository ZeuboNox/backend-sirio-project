package com.example.demo.categorie;


import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;


public interface CategorieRepository extends MongoRepository<Categorie, ObjectId> {
    public Optional<Categorie> findById(ObjectId idCategorie);
    public List<Categorie> findByName(String name);
}
